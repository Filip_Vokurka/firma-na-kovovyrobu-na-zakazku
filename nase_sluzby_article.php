    <article>
        <h2 class="nadpis">Svářením umíme vyrobit</h2>
        <h4>různorodé kovové konstrukce</h4>
        <h4>domácí nebo dílenský nábytek</h4>
        <h4>svařujeme ulomené nohy židlí</h4>
        <h4>ulomené části konstrukce postelí, kovového nábytku apod.</h4>
        <h2 class="nadpis">Kování</h2>
        <p>Vlastní vytváření výrobku stojí na několika základních technikách 
        a jejich vzájemné kombinaci, tyto techniky jsou v kovářství známy velmi
         dlouhou dobu, většina těchto technik byla známa ve starověku a znovu 
         objevena ve středověku. Samozřejmě záleží především na zručnosti
          kováře, jak dovede jednotlivé techniky kombinovat a jak precizně 
          je zvládne. Některé z těchto technik vyžadují značnou fyzickou sílu 
          a jsou poměrně namáhavé, u některých je nutností práce dvou lidí 
          (kováře a pomocníka). Staří kováři vypozorovali, že při ručním kování 
          velké série po mnoho hodin je výhodnější použít lehčího kladiva a dát 
          mu větší švih – přitahovat ho. Těžké kladivo je výhodné jen zdánlivě, 
          kovář se mnohem dříve unaví. Naši kováři zvládají věčinu těchto 
          technik a ruznými materiáli.</p>
    </article>