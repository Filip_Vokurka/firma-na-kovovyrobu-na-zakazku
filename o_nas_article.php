    <article>
        <h1>Co děláme</h1>
        <p>Naší hlavní činností je kovovýroba a zámečnictví. Vyrobíme
        Vám kovové výrobky přesně dle Vašich představ – na míru. Zabýváme se
        výrobou palet do robotických linek pro automobilový průmysl. </p>
        <h1>Řezání vodním paprskem</h1>
        <p>Prodádíme řezání materiálu s využitím 3D otočné hlavy jednak pro 
        maximální eliminaci podřezů, hlavně u silnějších materiálů, ale i pro 
        využití možnosti řezání pod úhlem, kdy je možno naklápět hlavu o ± 45°.
        Podstatou dělení materiálů je obrušování děleného materiálu tlakem 
        vodního paprsku. Tento proces je v podstatě stejný jako vodní eroze, 
        ale značně zrychlený a soustředěný do jednoho místa.
        Velkou výhodou při řezání vysokotlakým vodním paprskem je řezání bez
         tepelného ovlivnění řezaného materiálu, tzv. studený řez. Obráběný 
         díl nevykazuje fyzikální, chemické ani mechanické změny a je následně 
         snadno obrobitelný.</p>
         <img src="paprsek1.jpg" alt="vodní paprsek" class="paprsek">
         <img src="paprsek2.png" alt="vodní paprsek" class="paprsek">
         <img src="paprsek3.jpg" alt="vodní paprsek" class="paprsek">
    </article>